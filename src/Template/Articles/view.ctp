<h1><?= h($article->title) ?></h1>
<p><?= h($article->body) ?></p>
<p><small>Created: <?= h($article->created->format(DATE_RFC850)) ?></small></p>
<p><?= $this->Html->link('Edit', ['action' => 'edit', $article->slug]) ?></p>
<p><b>Tags:</b> <?= h($article->tag_string) ?></p>
<p><b>Comment:</b> </p>
<table>
<td>Comment</td>
<td>Date</td>
<td>User</td>

<?php foreach ($article->comments as $comment): ?>
    <tr>
        <td>
            <?= h($comment->comment) ?>
        </td>
        <td>
            <?= h($comment->created->format(DATE_RFC850)) ?>
        </td>
        <td>
        <!-- ['Controller'=>'Users','action'...] -->
        <?= $this->Html->link(h($comment->user->email), ['controller'=>'users','action' => 'view', $comment->user->id ]) ?>
        </td>
    </tr>
<?php endforeach; ?>
</table>
<p><b>New comment</b></p>
<?= $this->Form->create($addComment)?>
<?=    $this->Form->control('comment')?>
  <?=   $this->Form->button(__('Save comment'))?>
  <?=   $this->Form->end()?>


