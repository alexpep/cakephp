<?php
// src/Controller/ArticlesController.php

namespace App\Controller;

use App\Controller\AppController;
class ArticlesController extends AppController
{

	public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
	}
	
	public function index()
    	{
       	 $this->loadComponent('Paginator');
      	  $articles = $this->Paginator->paginate($this->Articles->find());
       	 $this->set(compact('articles'));
		}
		
	public function view($slug = null)
	{
		$article = $this->Articles->findBySlug($slug)->contain(['Tags','Comments'=> ['Users']])->firstOrFail();
	
		
		//--------------------- add new comments
		$addComment = $this->Articles->comments->newEntity();
		if ($this->request->is('post')) {
			$addComment = $this->Articles->comments->patchEntity($addComment, $this->request->getData());
	
			// Changed: Set the user_id from the session.
			$addComment->user_id = $this->Auth->user('id');
			$addComment->article_id = $article->id;
			
			//print($addComment); peut faire un comment

			if ($this->Articles->comments->save($addComment)) {
				$this->Flash->success(__('Your comment has been saved.'));
				return $this->redirect($this->referer());
			}
			if($this->Auth->user('id') == null)
            {
                $this->Flash->error(__('You need to be logged in to post a comment.'));
            } else {
                $this->Flash->error(__('The article could not be saved. Please, try again.'));
            }
		}
		//$this->set('addComment', $addComment);
		$this->set(compact('article','addComment'));
	}

	public function add()
    {
		$article = $this->Articles->newEntity();
		if ($this->request->is('post')) {
			$article = $this->Articles->patchEntity($article, $this->request->getData());
	
			// Changed: Set the user_id from the session.
			$article->user_id = $this->Auth->user('id');
	
			if ($this->Articles->save($article)) {
				$this->Flash->success(__('Your article has been saved.'));
				return $this->redirect(['action' => 'index']);
			}
			$this->Flash->error(__('Unable to add your article.'));
		}
		$this->set('article', $article);
	}
	
	public function edit($slug)
	{
		$article = $this->Articles
			->findBySlug($slug)
			->contain('Tags') // load associated Tags
			->firstOrFail();

		if ($this->request->is(['post', 'put'])) {
			$this->Articles->patchEntity($article, $this->request->getData(), [
				// Added: Disable modification of user_id.
				'accessibleFields' => ['user_id' => false]
			]);
			if ($this->Articles->save($article)) {
				$this->Flash->success(__('Your article has been updated.'));
				return $this->redirect(['action' => 'index']);
			}
			$this->Flash->error(__('Unable to update your article.'));
		}
		$this->set('article', $article);
	}

	public function delete($slug)
	{
		$this->request->allowMethod(['post', 'delete']);

		$article = $this->Articles->findBySlug($slug)->firstOrFail();
		if ($this->Articles->delete($article)) {
			$this->Flash->success(__('The {0} article has been deleted.', $article->title));
			return $this->redirect(['action' => 'index']);
		}
	}

	public function tags()
	{
		// The 'pass' key is provided by CakePHP and contains all
		// the passed URL path segments in the request.
		$tags = $this->request->getParam('pass');

		// Use the ArticlesTable to find tagged articles.
		$articles = $this->Articles->find('tagged', [
			'tags' => $tags
		]);

		// Pass variables into the view template context.
		$this->set([
			'articles' => $articles,
			'tags' => $tags
		]);
	}

	public function isAuthorized($user)
	{
			$action = $this->request->getParam('action');
		// The add and tags actions are always allowed to logged in users.
		if (in_array($action, ['add', 'tags'])) {
			return true;
		}

		// All other actions require a slug.
		$slug = $this->request->getParam('pass.0');
		if (!$slug) {
			return false;
		}

		// Check that the article belongs to the current user.
		$article = $this->Articles->findBySlug($slug)->first();

		return $article->user_id === $user['id'];
	}


}